<?php

/**
 * Classe para receber e utilizar o dados para criar um Vertical Menu.
 */
class VerticalMenuData{
    private $cor_fundo_menu;
    private $cor_texto;
    private $cor_hover;
    private $cor_ativo;
    private $itens;
   
    
    function __construct($item) {
        $this->cor_fundo_menu = isset($item->cor_fundo_menu) ? $item->cor_fundo_menu : null;
        $this->cor_texto = isset($item->cor_texto) ? $item->cor_texto : null;
        $this->cor_hover = isset($item->cor_hover) ? $item->cor_hover : null;
        $this->cor_ativo = isset($item->cor_ativo) ? $item->cor_ativo : null;
        $this->itens = isset($item->itens) ? $item->itens : null;
    }
    
    /**
     * Retorna o valor armazenado em cor_fundo_menu nesta classe.
     * @return string
     */
    public function cor_fundo_menu()
    {
        return $this->cor_fundo_menu;
    }
    
    /**
     * Retorna o valor armazenado em cor_texto nesta classe.
     * @return string
     */
    public function cor_texto()
    {
        return $this->cor_texto;
    }
    
    /**
     * Retorna o valor armazenado em cor_hover nesta classe.
     * @return string
     */
    public function cor_hover()
    {
        return $this->cor_hover;
    }
    
    /**
     * Retorna o valor armazenado em cor_ativo nesta classe.
     * @return string
     */
    public function cor_ativo()
    {
        return $this->cor_ativo;
    }
    
    /**
     * Retorna o valor armazenado em itens nesta classe.
     * @return string
     */
    public function itens()
    {
        return $this->itens;
    }
    
}