<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'libraries/Vertical_Menu/VerticalMenuLoader.php';

class VerticalMenu_Model extends CI_Model{
    
    /**
     * Pega o bloco do Vertical Menu com seu vertical menu, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getVerticalMenu()
    {
        $rs = (object)array();
        
        $menu = new VerticalMenuLoader();
        $rs->menu = $menu->getDadosMenu();
        $rs->code = $menu->gera_codigo_HTML();;
        $rs->dados = $menu->get_dados();
        $rs->test = $menu->get_test();
        
        return $rs;
    }
    
}