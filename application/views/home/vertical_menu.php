<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="section-heading h2 mb-3">Vertical Menu</h4>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-info" href="<?= base_url("test/VerticalMenu_Test") ?>">Teste do PHP</a>
                    </div>
                    <p class="description">Um menu com disponibilidade para utilização vertical dando mais espaço de tela e facilidade na utilização</p>
                </div>
                <p class="description">Neste menu é possivel definir a cor dos fundo, links(ativo e inativos) e do texto. Com um design pratico e bonito.</p>
                <p class="description">Nesta classe é possivel definir o a quantidade de itens do menu e os itens ativos.</p>
                <br/>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $menu->menu?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="verticalmenu-html-ex" data-toggle="tab" href="#verticalmenu-html" role="tab" aria-controls="verticalmenu-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="verticalmenu-classe-ex" data-toggle="tab" href="#verticalmenu-classe" role="tab" aria-controls="verticalmenu-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="verticalmenu-test-ex" data-toggle="tab" href="#verticalmenu-test" role="tab" aria-controls="verticalmenu-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="verticalmenu-html" role="tabpanel" aria-labelledby="verticalmenu-html-ex">
                            <?= $menu->code ?>
                        </div>
                        <div class="tab-pane fade" id="verticalmenu-classe" role="tabpanel" aria-labelledby="verticalmenu-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $menu->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="verticalmenu-test" role="tabpanel" aria-labelledby="verticalmenu-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($menu->test as $item): ?>
                                    <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>