<?php

class Home extends MY_Controller{
    
    function __construct()
    {
        parent:: __construct();
        $this->load->model("Button_Model", "m_button");
        $this->load->model("Modal_Model", "m_modal");
        $this->load->model("VerticalMenu_Model", "m_menu");
    }
    
    public function index()
    {
        $this->data = array();
        $this->conteudo["card"] = $this->load->view("home/index", null, true);
        
        $this->show($this->conteudo, $this->data);
    }

    public function button()
    {
        $this->data = array();
        $data["buttons_basicos"] = $this->m_button->getButtons_Basico();
        $data["buttons_adicionais"] = $this->m_button->getButton_Adicioanais();
        $data["buttons_gradient"] = $this->m_button->getButton_Gradient();
        $data["buttons_outline"] = $this->m_button->getButton_Outline();
        $data["buttons_icon"] = $this->m_button->getButton_Icon();
        $data["buttons_size"] = $this->m_button->getButton_Size();
        $data["buttons_active"] = $this->m_button->getButton_Active();
        $data["buttons_disabled"] = $this->m_button->getButton_Disabled();
        $this->conteudo["conteudo"] = $this->load->view("home/button", $data, true);

        $this->show($this->conteudo, $this->data);
    }
    
    public function modal()
    {
        $this->data = array();
        
        $data["modal_type"] = $this->m_modal->getModal_type();
        $data["modal_side"] = $this->m_modal->getModal_side();
        $data["modal_fluid"] = $this->m_modal->getModal_fluid();
        $data["modal_frame"] = $this->m_modal->getModal_frame();
        
        $this->conteudo["conteudo"] = $this->load->view("home/modal", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    
    public function Vertical_menu()
    {
        $this->data = array();
        
        $data["menu"] = $this->m_menu->getVerticalMenu();
        
        $this->conteudo["conteudo"] = $this->load->view("home/vertical_menu", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    
    public function phpdocumentor()
    {
        $this->data = array();
        
        $this->conteudo["php"] = "<iframe src='".base_url("./documentos/index.html")."' class='w-100 b-0' style='height:100vh;'></iframe>";
        
        $this->show($this->conteudo, $this->data);
    }
}