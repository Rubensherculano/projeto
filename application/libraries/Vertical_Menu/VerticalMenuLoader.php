<?php

include_once 'VerticalMenuItem.php';
include_once 'VerticalMenuData.php';
include_once 'VerticalMenuTest.php';

/**
 * Classe para criar um Menu Vertical, onde são passandos os dados do Vertical Menu para as classes de criação.
 */
class VerticalMenuLoader{

    private $menuData;
    private $test;
    
    /**
     * Monta a estrutura do Vertical Menu(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosMenu()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_menu();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->cor_fundo_menu == false || $test[$key]->test->cor_texto == false || $test[$key]->test->cor_hover == false || $test[$key]->test->cor_ativo == false || $test[$key]->test->itens == false)
            {
                $verf = $key;
            }
        }
        
        $this->menuData = $array;
        $this->test = $test;
        
        $html = '';
        
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $menu = new VerticalMenuData($item);
                $component = new VerticalMenuItem($menu);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
    }
    
    /**
     * Pega o codigo HTML das estruturas do Menu, para mostrar como linha de codigo.
     * @return $html: string
     */
    public function gera_codigo_HTML()
    {
        $html = '';
        foreach($this->menuData as $item)
        {
            $menu = new VerticalMenuData($item);
            $component = new VerticalMenuItem($menu);
            $html .= $component->getCodigoHTML();
        }
        
        return $html;
    }

    /**
     * Pega o card com informações que foram utilizadas na classe.
     * @return $html: string
     */
    public function get_dados()
    {
        $html = '';
        
        foreach($this->menuData as $item)
        {
            $button = new VerticalMenuData($item);
            $component = new VerticalMenuItem($button);
            $html .= $component->get_Dados();
        }

        return $html;
    }
    
    /**
     * Pega o card com as informações que foram passada para a classe, mostrando no resultado o que foi preenchido.
     * @param $dados: array variavel com os dados de um Vertical Menu.
     * @return $html: string
     */
    public function verifica_test($dados)
    {
        $html =  (object)array();
        $test = new VerticalMenuTest($dados);
        
        $array = (object)array();
        $array->cor_fundo_menu = $test->verifica_cor_fundo_menu();
        $array->cor_texto = $test->verifica_cor_texto();
        $array->cor_hover = $test->verifica_cor_hover();
        $array->cor_ativo = $test->verifica_cor_ativo();
        $array->itens = $test->verifica_itens();
        
        $html->test = $array;
        $html->html = $test->get_test($array);
        
        return $html;
    }
    
    /**
     * Retorna o card do teste do Menu.
     * @return string
     */
    public function get_test()
    {
        return $this->test;
    }
    
    /**
     * Dados a serem passados para classe de Vertical Menu.
     * @return $array :array
     */
    public function get_menu()
    {
        $array = array(
            "0" => (object)array(
                "cor_fundo_menu" => "LightGray",
                "cor_texto" => "Black",
                "cor_hover" => "Grey",
                "cor_ativo" => "LimeGreen",
                "itens" => array(
                    "0" => (object)array(
                        "nome" => "Home",
                        "link" => "#",
                        "ativo" => "active"
                    ),
                    "1" => (object)array(
                        "nome" => "Link",
                        "link" => "#",
                        "ativo" => ""
                    ),
                    "2" => (object)array(
                        "nome" => "Link",
                        "link" => "#",
                        "ativo" => ""
                    ),
                    "3" => (object)array(
                        "nome" => "Link",
                        "link" => "#",
                        "ativo" => ""
                    ),
                )
            )
        );
        
        return $array;
    }
}