<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include_once APPPATH. '/libraries/Vertical_Menu/VerticalMenuLoader.php';

class VerticalMenu_Test extends MyToast {
    
    function __construct() {
        parent::__construct('VerticalMenu_Test');
    }
    
    /**
     * Realiza testes com a variavel cor_fundo_menu(controller).
     * @return boolean
     */
    function test_verifica_cor_fundo_menu()
    {
        $button = new VerticalMenuLoader();
        $info = $button->get_menu()[0];
        $result = strlen($info->cor_fundo_menu) > 3 ? true : false;
        $this->_assert_true($result, "O Cor de Fundo do Menu precisa estar preenchido e ter mais que 3 caracteres");
    }
    
    /**
     * Realiza testes com a variavel cor_texto(controller).
     * @return boolean
     */
    function test_verifica_cor_texto()
    {
        $button = new VerticalMenuLoader();
        $info = $button->get_menu()[0];
        $result = strlen($info->cor_texto) > 3 ? true : false;
        $this->_assert_true($result, "A Cor do texto precisa estar preenchido e ter mais que 3 caracteres");
    }
    
    /**
     * Realiza testes com a variavel cor_hover(controller).
     * @return boolean
     */
    function test_verifica_cor_hover()
    {
        $button = new VerticalMenuLoader();
        $info = $button->get_menu()[0];
        $result = strlen($info->cor_hover) > 3 ? true : false;
        $this->_assert_true($result, "A Cor do Hover precisa estar preenchido e ter mais que 3 caracteres");
    }
    
    /**
     * Realiza testes com a variavel cor_ativo(controller).
     * @return boolean
     */
    function test_verifica_cor_ativo()
    {
        $button = new VerticalMenuLoader();
        $info = $button->get_menu()[0];
        $result = strlen($info->cor_ativo) > 3 ? true : false;
        $this->_assert_true($result, "A Cor do Menu Ativo precisa estar preenchido e ter mais que 3 caracteres");
    }
    
    /**
     * Realiza testes com a variavel itens(controller).
     * @return boolean
     */
    function test_verifica__itens()
    {
        $button = new VerticalMenuLoader();
        $info = $button->get_menu()[0];
        $result = !empty($info->itens) ? true : false;
        $this->_assert_true($result, "Os itens deve existir na classe");
    }
    
}