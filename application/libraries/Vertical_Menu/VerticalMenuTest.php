<?php

/**
 * Classe com o teste dos dados que serão usados no Vertical Menu.
 */
class VerticalMenuTest{
    private $cor_fundo_menu;
    private $cor_texto;
    private $cor_hover;
    private $cor_ativo;
    private $itens;
   
    
    function __construct($item) {
        $this->cor_fundo_menu = isset($item->cor_fundo_menu) ? $item->cor_fundo_menu : null;
        $this->cor_texto = isset($item->cor_texto) ? $item->cor_texto : null;
        $this->cor_hover = isset($item->cor_hover) ? $item->cor_hover : null;
        $this->cor_ativo = isset($item->cor_ativo) ? $item->cor_ativo : null;
        $this->itens = isset($item->itens) ? $item->itens : null;
    }
    
    /**
     * Realiza testes com a variavel cor_fundo_menu.
     * @return boolean
     */
    public function verifica_cor_fundo_menu()
    {
        if(!empty($this->cor_fundo_menu) && strlen($this->cor_fundo_menu) > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel cor_texto.
     * @return boolean
     */
    public function verifica_cor_texto()
    {
        if(!empty($this->cor_texto) && strlen($this->cor_texto) > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel cor_hover.
     * @return boolean
     */
    public function verifica_cor_hover()
    {
        if(!empty($this->cor_hover) && strlen($this->cor_hover) > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel cor_ativo.
     * @return boolean
     */
    public function verifica_cor_ativo()
    {
        if(!empty($this->cor_ativo) && strlen($this->cor_ativo) > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel itens.
     * @return boolean
     */
    public function verifica_itens()
    {
        if(!empty($this->itens))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Monta um card com as informações testada da classe.
     * @param $dados : array dados do Vertical Menu
     * @return $html : string
     */
    public function get_test($dados)
    {
        $html = '';
        
        $html .= '<div class="col-md-12">';
        $html .= '<div class="card border-dark mr-1 ml-1 mt-3 mb-3">';
        $html .= '<div class="card-body">';
        $html .= '<h5 class="card-title text-primary">Menu</h5>';
        $html .= '<p class="card-title">Cor de Fundo do Menu: '.($dados->cor_fundo_menu == true ? '<span class="text-primary">Passo - Cor de Fundo do menu com 3 caracteres pelo menos</span>' : '<span class="text-danger">A Cor de Fundo do menu esta vazio ou com menos que 3 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Cor de Texto: '.($dados->cor_texto == true ? '<span class="text-primary">Passo - Cor do Texto Preenchido e com 3 caracteres pelo menos</span>' : '<span class="text-danger">A Cor do texto esta vazia ou com menos que 3 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Cor Hover: '.($dados->cor_hover == true ? '<span class="text-primary">Passo - Cor Hover Preenchido e com 3 caracteres pelo menos</span>' : '<span class="text-danger">A Cor do hover esta vazia ou com menos que 3 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Cor Ativo: '.($dados->cor_ativo == true ? '<span class="text-primary">Passo - Cor Ativo Preenchido e com 4 caracteres pelo menos</span>' : '<span class="text-danger">A Cor do ativo esta vazia ou com menos que 3 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Itens: '.($dados->itens == true ? '<span class="text-primary">Passo - Itens Preenchido</span>' : '<span class="text-danger">Os Itens está vazio ou com menos que 3 caracteres</span>').'</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        
        return $html;
    }
}