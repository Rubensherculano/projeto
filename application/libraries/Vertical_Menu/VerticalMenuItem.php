<?php

include_once APPPATH.'libraries/Component.php';
include_once 'VerticalMenuData.php';

/**
 * Classe para criar um botão com diferentes caracteristicas.
 */
class VerticalMenuItem extends Component{

    private $dados;
    
    function __construct(VerticalMenuData $dados) {
        $this->dados = $dados;
    }
    
    /**
     * Monta o codigo html de um Vertical Menu.
     * @return $html : string
     */
    public function getHTML()
    {
        $html = '<style>
            .vertical-menu {
                width: 200px;
            }

            .vertical-menu a {
                background-color: '.$this->dados->cor_fundo_menu().';
                color: '.$this->dados->cor_texto().';
                display: block;
                padding: 12px;
                text-decoration: none;
            }

            .vertical-menu a:hover {
                background-color: '.$this->dados->cor_hover().';
            }

            .vertical-menu a.active {
                background-color: '.$this->dados->cor_ativo().';
                color: white;
            }
        </style>';
        $html .= '<div class="vertical-menu">';
        foreach($this->dados->itens() as $item)
        {
            $html.= '<a href="'.$item->link.'" class="'.$item->ativo.'">'.$item->nome.'</a>';
        }
        
        $html .= '</div>';
        
        return $html;
    }
    
    /**
     * Montar o texto em html de como formar um Vertical Menu com determinadas caracteristicas.
     * @return $html : string
     */
    public function getCodigoHTML()
    {
        $html = '';
        $html .= '<span class="indigo-text">&lt;style&gt;</span><br/>
            <span class="deep-orange-text">.vertical-menu</span> {<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">width:</span> <span class="blue-text">200px</span>;<br/>
            }<br/><br/>

            <span class="deep-orange-text">.vertical-menu a </span>{<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">background-color:</span> <span class="blue-text">'.$this->dados->cor_fundo_menu().'</span>;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">color:</span> <span class="blue-text">'.$this->dados->cor_texto().'</span>;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">display:</span> <span class="blue-text">block</span>;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">padding:</span> <span class="blue-text">12px</span>;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">text-decoration:</span> <span class="blue-text">none</span>;<br/>
            }<br/><br/>

            <span class="deep-orange-text">.vertical-menu a:hover </span>{<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">background-color:</span> <span class="blue-text">'.$this->dados->cor_hover().'</span>;<br/>
            }<br/>

            <span class="deep-orange-text">.vertical-menu a.active </span>{<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">background-color:</span> <span class="blue-text">'.$this->dados->cor_ativo().'</span>;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text">color:</span> <span class="blue-text">white</span>;<br/>
            }<br/>
        <span class="indigo-text">&lt/style&gt;</span><br/><br/>';
        $html .= '<span class="indigo-text">&lt;div <span class="blue-text">class="<span class="red-text">vertical-menu</span>"</span>&gt;</span><br/>';
        foreach($this->dados->itens() as $item)
        {
            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;a <span class="blue-text">href="<span class="red-text">'.$item->link.'</span>" class="<span class="red-text">'.$item->ativo.'</span>"</span>&gt;</span>'.$item->nome.'<span class="indigo-text">&lt;/a&gt;</span><br/>';
        }
        $html .= '<span class="indigo-text">&ltdiv&gt;</span>';
        
        return $html;
    }
    
    /**
     * Monta um card com informações que foram passadas para a classe.
     * @return $html : string
     */
    public function get_Dados()
    {
        $html = '';
        
        $html .= '<div class="col-md-12">';
        $html .= '<div class="card border-dark mr-1 ml-1 mt-3 mb-3">';
        $html .= '<div class="card-body">';
        $html .= '<h5 class="card-title text-primary">Menu</h5>';
        $html .= '<p class="card-title">Cor de Fundo do Menu - '.$this->dados->cor_fundo_menu().'</p>';
        $html .= '<p class="card-title">Cor do Texto - '.$this->dados->cor_texto().'</p>';
        $html .= '<p class="card-title">Cor do Hover - '.$this->dados->cor_hover().'</p>';
        $html .= '<p class="card-title">Cor do Ativo - '.$this->dados->cor_ativo().'</p>';
        $html .= '<p class="card-title">Itens - '.(!empty($this->dados->itens()) ? "Array com os dados do item" : "Sem item no Menu").'</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        
        return $html;
    }
}