<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'libraries/Button/ButtonLoader.php';

class Button_Model extends CI_Model{

    /**
     * Pega o bloco do botão basico com seus botões, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getButtons_Basico()
    {
        $rs = (object)array();
        
        $button = new ButtonLoader();
        $rs->button = $button->getDadosBotao_Basicos();
        $rs->code = $button->gera_codigo_HTML();
        $rs->dados = $button->get_dados();
        $rs->test = $button->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do botão adicionais com seus botões, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getButton_Adicioanais()
    {
        $rs = (object)array();
        
        $button = new ButtonLoader();
        $rs->button = $button->getDadosBotao_Adicionais();
        $rs->code = $button->gera_codigo_HTML();
        $rs->dados = $button->get_dados();
        $rs->test = $button->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do botão gradiente com seus botões, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getButton_Gradient()
    {
        $rs = (object)array();
        
        $button = new ButtonLoader();
        $rs->button = $button->getDadosBotao_Gradient();
        $rs->code = $button->gera_codigo_HTML();
        $rs->dados = $button->get_dados();
        $rs->test = $button->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do botão outline com seus botões, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getButton_Outline()
    {
        $rs = (object)array();
        
        $button = new ButtonLoader();
        $rs->button = $button->getDadosBotao_Outline();
        $rs->code = $button->gera_codigo_HTML();
        $rs->dados = $button->get_dados();
        $rs->test = $button->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do botão icone com seus botões, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getButton_Icon()
    {
        $rs = (object)array();
        
        $button = new ButtonLoader();
        $rs->button = $button->getDadosBotao_Icon();
        $rs->code = $button->gera_codigo_HTML();
        $rs->dados = $button->get_dados();
        $rs->test = $button->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do tamanho do botão com seus botões, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getButton_Size()
    {
        $rs = (object)array();
        
        $button = new ButtonLoader();
        $rs->button = $button->getDadosBotao_Size();
        $rs->code = $button->gera_codigo_HTML();
        $rs->dados = $button->get_dados();
        $rs->test = $button->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do botão ativo com seus botões, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getButton_Active()
    {
        $rs = (object)array();
        
        $button = new ButtonLoader();
        $rs->button = $button->getDadosBotao_Active();
        $rs->code = $button->gera_codigo_HTML();
        $rs->dados = $button->get_dados();
        $rs->test = $button->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do botão desativado com seus botões, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getButton_Disabled()
    {
        $rs = (object)array();
        
        $button = new ButtonLoader();
        $rs->button = $button->getDadosBotao_Disabled();
        $rs->code = $button->gera_codigo_HTML();
        $rs->dados = $button->get_dados();
        $rs->test = $button->get_test();
        
        return $rs;
    }

}