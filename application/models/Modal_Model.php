<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'libraries/Modal/ModalLoader.php';

class Modal_Model extends CI_Model{
    
    /**
     * Pega o bloco do tipo de modal com seus modals, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getModal_type()
    {
        $rs = (object)array();
        
        $modal = new ModalLoader();
        $rs->modal = $modal->getDadosModal_type();
        $rs->code = $modal->gera_codigo_HTML();
        $rs->dados = $modal->get_dados();
        $rs->test = $modal->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do modal lateral com seus modals, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getModal_side()
    {
        $rs = (object)array();
        
        $modal = new ModalLoader();
        $rs->modal = $modal->getDadosModal_side();
        $rs->code = $modal->gera_codigo_HTML();
        $rs->dados = $modal->get_dados();
        $rs->test = $modal->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do fluid modal com seus modals, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getModal_fluid()
    {
        $rs = (object)array();
        
        $modal = new ModalLoader();
        $rs->modal = $modal->getDadosModal_fluid();
        $rs->code = $modal->gera_codigo_HTML();
        $rs->dados = $modal->get_dados();
        $rs->test = $modal->get_test();
        
        return $rs;
    }
    
    /**
     * Pega o bloco do modal frame com seus modals, seu codigo HTML, os dados que foram passados para classe e seus testes.
     * @return $rs : array
     */
    public function getModal_frame()
    {
        $rs = (object)array();
        
        $modal = new ModalLoader();
        $rs->modal = $modal->getDadosModal_frame();
        $rs->code = $modal->gera_codigo_HTML();
        $rs->dados = $modal->get_dados();
        $rs->test = $modal->get_test();
        
        return $rs;
    }
    
}

