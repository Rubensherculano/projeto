<?php

/**
* Classe com o teste dos dados que serão usados em um botão
*/
class ButtonTest{
    
    private $titulo;
    private $classe;
    private $comentario;
    private $icon_left;
    private $icon_right;
    private $type;
    private $size;
    private $active;
    private $disabled;
    
    function __construct($item) {
        $this->titulo = isset($item->titulo) ? $item->titulo : null;
        $this->classe = isset($item->classe) ? $item->classe : null;
        $this->comentario = isset($item->comentario) ? $item->comentario : null;
        $this->icon_left = isset($item->icon_left) ? $item->icon_left : null;
        $this->icon_right = isset($item->icon_right) ? $item->icon_right : null;
        $this->type = isset($item->type) ? $item->type : null;
        $this->size = isset($item->size) ? $item->size : null;
        $this->active = isset($item->active) ? $item->active : null;
        $this->disabled = isset($item->disabled) ? $item->disabled : null;
    }
    
    /**
     * Realiza testes com a variavel titulo.
     * @return boolean
     */
    public function verifica_titulo()
    {
        if(!empty($this->titulo) && strlen($this->titulo) > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel classe.
     * @return boolean
     */
    public function verifica_classe()
    {
        if(!empty($this->classe) && strlen($this->titulo) > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel comentario.
     * @return boolean
     */
    public function verifica_comentario()
    {
        if($this->comentario != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel icone esquerdo.
     * @return boolean
     */
    public function verifica_icon_left()
    {
        if($this->icon_left !== null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel icone direito.
     * @return boolean
     */
    public function verifica_icon_right()
    {
        if($this->icon_right !== null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel tipo.
     * @return boolean
     */
    public function verifica_type()
    {
        if(!empty($this->type) && strlen($this->type) > 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel tamanho.
     * @return boolean
     */
    public function verifica_size()
    {
        if($this->size !== null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel ativo.
     * @return boolean
     */
    public function verifica_active()
    {
        if($this->active !== null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel desativada.
     * @return boolean
     */
    public function verifica_disabled()
    {
        if($this->disabled !== null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Monta um card com as informações testada da classe.
     * @param $dados : array dados do botão
     * @return $html : string
     */
    public function get_test($dados)
    {
        $html = '';
        
        $html .= '<div class="col-md-6">';
        $html .= '<div class="card border-dark mr-1 ml-1 mt-3 mb-3">';
        $html .= '<div class="card-body">';
        $html .= '<h5 class="card-title text-primary">Botão: '.$dados->titulo_button.'</h5>';
        $html .= '<p class="card-title">Titulo: '.($dados->titulo == true ? '<span class="text-primary">Passo - Titulo Preenchido e com 3 caracteres pelo menos</span>' : '<span class="text-danger">O titulo esta vazio ou com menos que 3 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Classe: '.($dados->classe == true ? '<span class="text-primary">Passo - Classe Preenchido e com 4 caracteres pelo menos</span>' : '<span class="text-danger">A classe esta vazia ou com menos que 4 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Icone Esquerdo: '.($dados->icon_left == true ? '<span class="text-primary">Passo - Icon Esquerdo esta presente na classe</span>' : '<span class="text-danger">Não foi passado informação para o icone esquerdo(Necessário mesmo que a informação esteja vazia)</span>').'</p>';
        $html .= '<p class="card-title">Icone Direito: '.($dados->icon_right == true ? '<span class="text-primary">Passo - Icone Direito esta presente na classe</span>' : '<span class="text-danger">Não foi passado informação para o icone direito (Necessario mesmo que a informação esteja vazia)</span>').'</p>';
        $html .= '<p class="card-title">Tipo: '.($dados->type == true ? '<span class="text-primary">Passo - Tipo de botão Preenchido e com 4 caracteres pelo menos</span>' : '<span class="text-danger">O tipo de botão está vazio ou com menos que 4 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Tamanho: '.($dados->size == true ? '<span class="text-primary">Passo - O tamanho do botão esta presente na classe</span>' : '<span class="text-danger">Não foi passado informação para o tamanho do botão (Necessário mesmo que a informação esteja vazia)</span>').'</p>';
        $html .= '<p class="card-title">Comentario: '.($dados->comentario == true ? '<span class="text-primary">Passo - Comentario esta presente na classe</span>' : '<span class="text-danger">Não foi passado informação para comentario (Necessario mesmo que a infomação esteja vazia</span>').'</p>';
        $html .= '<p class="card-title">Ativo: '.($dados->active == 1 ? '<span class="text-primary">Passo - Active preenchido</span>' : '<span class="text-danger">Não foi passado informação para active(verdadeiro ou falso)</span>').'</p>';
        $html .= '<p class="card-title">Desativado: '.($dados->disabled == 1 ? '<span class="text-primary">Passo - Disabled preenchido</span>' : '<span class="text-danger">Não foi passado informação para disabled (verdadeiro ou false)</span>').'</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        
        return $html;
    }
}
