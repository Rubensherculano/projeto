<?php

/**
 * Classe para setar os dados de um modal em variaveis.
 */
class ModalData{
    private $titulo;
    private $nome;
    private $tamanho;
    private $position;
    private $efeito;
    private $conteudo;
    
    function __construct($item) {
        $this->titulo = $item->titulo;
        $this->nome = $item->nome;
        $this->tamanho = $item->tamanho;
        $this->position = $item->position;
        $this->efeito = $item->efeito;
        $this->conteudo = $item->conteudo;
    }
    
    /**
     * Retorna o valor armazenado em titulo nesta classe.
     * @return string
     */
    public function titulo()
    {
        return $this->titulo;
    }
    
    /**
     * Retorna o valor armazenado em nome nesta classe.
     * @return string
     */
    public function nome()
    {
        return $this->nome;
    }
    
    /**
     * Retorna o valor armazenado em tamanho nesta classe.
     * @return string
     */
    public function tamanho()
    {
        return $this->tamanho;
    }
    
    /**
     * Retorna o valor armazenado em position nesta classe.
     * @return string
     */
    public function position()
    {
        return $this->position;
    }
    
    /**
     * Retorna o valor armazenado em efeito nesta classe.
     * @return string
     */
    public function efeito()
    {
        return $this->efeito;
    }
    
    /**
     * Retorna o valor armazenado em conteudo nesta classe.
     * @return string
     */
    public function conteudo()
    {
        return $this->conteudo;
    }
    
}