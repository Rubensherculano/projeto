<?php

include_once 'ModalItem.php';
include_once 'ModalData.php';
include_once 'ModalTest.php';

/**
 * Classe para criar blocos de modals diferentes, onde são passandos os dados do modal para as classes de criação.
 */
class ModalLoader{

    private $modalData;
    private $test;
    
    /**
     * Monta a estrutura dos tipos de modals(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosModal_type()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_modal_type();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->nome == false || $test[$key]->test->tamanho == false || $test[$key]->test->position == false || $test[$key]->test->efeito == false || $test[$key]->test->conteudo == false)
            {
                $verf = $key;
            }
        }
        
        $this->modalData = $array;
        $this->test = $test;
        
        $html = '';
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $modal = new ModalData($item);
                $component = new ModalItem($modal);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
    }
    
    /**
     * Monta a estrutura dos modals laterais(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosModal_side()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_modal_side();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->nome == false || $test[$key]->test->tamanho == false || $test[$key]->test->position == false || $test[$key]->test->efeito == false || $test[$key]->test->conteudo == false)
            {
                $verf = $key;
            }
        }
        
        $this->modalData = $array;
        $this->test = $test;
        
        $html = '';
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $modal = new ModalData($item);
                $component = new ModalItem($modal);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
    }
    
    /**
     * Monta a estrutura dos modals fluids(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosModal_fluid()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_modal_fluid();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->nome == false || $test[$key]->test->tamanho == false || $test[$key]->test->position == false || $test[$key]->test->efeito == false || $test[$key]->test->conteudo == false)
            {
                $verf = $key;
            }
        }
        
        $this->modalData = $array;
        $this->test = $test;
        
        $html = '';
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $modal = new ModalData($item);
                $component = new ModalItem($modal);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
    }
    
    /**
     * Monta a estrutura dos modals de frame(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosModal_frame()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_modal_frame();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->nome == false || $test[$key]->test->tamanho == false || $test[$key]->test->position == false || $test[$key]->test->efeito == false || $test[$key]->test->conteudo == false)
            {
                $verf = $key;
            }
        }
        
        $this->modalData = $array;
        $this->test = $test;
        
        $html = '';
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $modal = new ModalData($item);
                $component = new ModalItem($modal);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
    }
    
    /**
     * Pega o codigo HTML das estruturas de cada modal, para mostrar como linha de codigo em HTML.
     * @return $html: string
     */
    public function gera_codigo_HTML()
    {
        $html = '';
        foreach($this->modalData as $item)
        {
            $modal = new ModalData($item);
            $component = new ModalItem($modal);
            $html .= $component->getCodigoHTML();
        }
        
        return $html;
    }
    
    /**
     * Pega o card com informações do dados que foram utilizadas na classe.
     * @return $html: string
     */
    public function get_dados()
    {
        $html = '';
        
        foreach($this->modalData as $item)
        {
            $modal = new ModalData($item);
            $component = new ModalItem($modal);
            $html .= $component->get_Dados();
        }

        return $html;
    }
    
    /**
     * Pega o card com as informações do dados que foram passada para a classe, mostrando no resultado do que foi preenchido.
     * @param $dados: array variavel com os dados de um modal.
     * @return $html: string
     */
    public function verifica_test($dados)
    {
        $html =  (object)array();
        $test = new ModalTest($dados);
        
        $array = (object)array();
        $array->titulo = $test->verifica_titulo();
        $array->nome = $test->verifica_nome();
        $array->tamanho = $test->verifica_tamanho();
        $array->position = $test->verifica_position();
        $array->efeito = $test->verifica_efeito();
        $array->conteudo = $test->verifica_conteudo();
        $array->titulo_modal = $dados->titulo;
        
        $html->test = $array;
        $html->html = $test->get_test($array);
        
        return $html;
    }
    
    /**
     * Retorna o card do teste de cada modal.
     * @return string
     */
    public function get_test()
    {
        return $this->test;
    }
    
    /**
     * Dados a serem passados para classe de modal, para ficarem com as informações do tipo de modal.
     * @return $array :array
     */
    public function get_modal_type()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Modal Medio",
                "nome" => "modal_medio",
                "tamanho" => "modal-md",
                "position" => "modal-dialog-centered",
                "efeito" => "modal-dialog",
                "conteudo" => "Conteudo do modal",
            ),
            "1" => (object)array(
                "titulo" => "Modal Pequeno",
                "nome" => "modal_pequeno",
                "tamanho" => "modal-sm",
                "position" => "modal-dialog-centered",
                "efeito" => "modal-dialog",
                "conteudo" => "Conteudo do modal",
            ),
            "2" => (object)array(
                "titulo" => "Modal Grande",
                "nome" => "modal_grande",
                "tamanho" => "modal-lg",
                "position" => "modal-dialog-centered",
                "efeito" => "modal-dialog",
                "conteudo" => "Conteudo do modal",
            ),
            "3" => (object)array(
                "titulo" => "Modal Fluid",
                "nome" => "modal_fluid",
                "tamanho" => "modal-fluid",
                "position" => "modal-dialog-centered",
                "efeito" => "modal-dialog",
                "conteudo" => "Conteudo do modal",
            ),
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de modal, para ficarem com as informações do modal lateral.
     * @return $array :array
     */
    public function get_modal_side()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Modal Superior Direito",
                "nome" => "modal_superior_direito",
                "tamanho" => "modal-sm",
                "position" => "modal-top-right",
                "efeito" => "modal-side",
                "conteudo" => "Conteudo do modal",
            ),
            "1" => (object)array(
                "titulo" => "Modal Superior Esquerdo",
                "nome" => "modal_superior_esquerdo",
                "tamanho" => "modal-sm",
                "position" => "modal-top-left",
                "efeito" => "modal-side",
                "conteudo" => "Conteudo do modal",
            ),
            "2" => (object)array(
                "titulo" => "Modal Inferior Direito",
                "nome" => "modal_inferior_direito",
                "tamanho" => "modal-sm",
                "position" => "modal-bottom-right",
                "efeito" => "modal-side",
                "conteudo" => "Conteudo do modal",
            ),
            "3" => (object)array(
                "titulo" => "Modal Inferior Esquerdo",
                "nome" => "modal_inferior_esquerdo",
                "tamanho" => "modal-sm",
                "position" => "modal-bottom-left",
                "efeito" => "modal-side",
                "conteudo" => "Conteudo do modal",
            ),
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de modal, para ficarem com as informações do modal fluid.
     * @return $array :array
     */
    public function get_modal_fluid()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Modal Fluid Direita",
                "nome" => "modal_fluid_direito",
                "tamanho" => "modal-sm",
                "position" => "modal-right",
                "efeito" => "modal-full-height",
                "conteudo" => "Conteudo do modal",
            ),
            "1" => (object)array(
                "titulo" => "Modal Fluid Esquerdo",
                "nome" => "modal_fluid_esquerdo",
                "tamanho" => "modal-sm",
                "position" => "modal-left",
                "efeito" => "modal-full-height",
                "conteudo" => "Conteudo do modal",
            ),
            "2" => (object)array(
                "titulo" => "Modal Fluid Inferior",
                "nome" => "modal_fluid_inferior",
                "tamanho" => "modal-sm",
                "position" => "modal-bottom",
                "efeito" => "modal-full-height",
                "conteudo" => "Conteudo do modal",
            ),
            "3" => (object)array(
                "titulo" => "Modal Fluid Superior",
                "nome" => "modal_fluid_superior",
                "tamanho" => "modal-sm",
                "position" => "modal-top",
                "efeito" => "modal-full-height",
                "conteudo" => "Conteudo do modal",
            ),
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de modal, para ficarem com as informações do modal frame.
     * @return $array :array
     */
    public function get_modal_frame()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Modal Frame Inferior",
                "nome" => "modal_frame_inferior",
                "tamanho" => "modal-sm",
                "position" => "modal-bottom",
                "efeito" => "modal-frame",
                "conteudo" => "Conteudo do modal",
            ),
            "1" => (object)array(
                "titulo" => "Modal Frame Direito",
                "nome" => "modal_frame_direito",
                "tamanho" => "modal-sm",
                "position" => "modal-top",
                "efeito" => "modal-frame",
                "conteudo" => "Conteudo do modal",
            ),
        );
        
        return $array;
    }
    
}