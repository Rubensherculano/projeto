<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include_once APPPATH. '/libraries/Button/ButtonLoader.php';

class Button_Test extends MyToast {
    
    function __construct() {
        parent::__construct('Button_Test');
    }
    
    /**
     * Realiza testes com a variavel titulo(controller).
     * @return boolean
     */
    function test_verifica_titulo_button()
    {
        $button = new ButtonLoader();
        $info = $button->get_botoes_basicos()[0];
        $result = strlen($info->titulo) > 3 ? true : false;
        $this->_assert_true($result, "O titulo precisa estar preenchido e ter mais que 3 caracteres");
    }
    
    /**
     * Realiza testes com a variavel classe(controller).
     * @return boolean
     */
    function test_verifica_classe_button()
    {
        $button = new ButtonLoader();
        $info = $button->get_botoes_basicos()[0];
        $result = strlen($info->classe) > 4 ? true : false;
        $this->_assert_true($result, "A classe precisa estar preenchido e ter mais que 4 caracteres");
    }
    
    /**
     * Realiza testes com a variavel comentario(controller).
     * @return boolean
     */
    function test_verifica_comentario_button()
    {
        $button = new ButtonLoader();
        $info = $button->get_botoes_basicos()[0];
        $result = isset($info->comentario) ? true : false;
        $this->_assert_true($result, "O comentario deve existir na classe(mesmo que vazia)");
    }
    
    /**
     * Realiza testes com a variavel icone esquerdo(controller).
     * @return boolean
     */
    function test_verifica_icon_left_button()
    {
        $button = new ButtonLoader();
        $info = $button->get_botoes_basicos()[0];
        $result = isset($info->icon_left) ? true : false;
        $this->_assert_true($result, "O icone esquerdo deve existir na classe(mesmo que vazio)");
    }
    
    /**
     * Realiza testes com a variavel icone direito(controller).
     * @return boolean
     */
    function test_verifica_icon_right_button()
    {
        $button = new ButtonLoader();
        $info = $button->get_botoes_basicos()[0];
        $result = isset($info->icon_right) ? true : false;
        $this->_assert_true($result, "O icone direito deve existir na classe(mesmo que vazio)");
    }
    
    /**
     * Realiza testes com a variavel tipo(controller).
     * @return boolean
     */
    function test_verifica_type_button()
    {
        $button = new ButtonLoader();
        $info = $button->get_botoes_basicos()[0];
        $result = strlen($info->type) > 4 ? true : false;
        $this->_assert_true($result, "O tipo do button precisa ser especificado e ter mais que 4 caracteres");
    }
    
    /**
     * Realiza testes com a variavel tamanho(controller).
     * @return boolean
     */
    function test_verifica_size_button()
    {
        $button = new ButtonLoader();
        $info = $button->get_botoes_basicos()[0];
        $result = isset($info->size) ? true : false;
        $this->_assert_true($result, "O tamanho do botão precisa existir na classe(mesmo que vazio)");
    }
    
}
