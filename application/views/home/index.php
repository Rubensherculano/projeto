<main class="text-center py-5">

    <div class="container">
        <div class="row">
            <img src="<?= base_url("assets/img/wallpaper_nintendo.jpg") ?>" class="img-fluid rounded mx-auto" alt="Responsive image">
        </div>
        <p class="h2-responsive text-center titulo_index">Projeto</p>
        <div class="row">
            <div class="col-md-4">
                <img src="https://mdbootstrap.com/img/Others/documentation/img%20(55)-mini.jpg" class="img-fluid" alt="Logo" />
            </div>
            <div class="col-md-8">

                <p align="justify">Projeto desenvolvido com intuito de estudo sobre a criação de componentes e separação de responsabilidade.</p>
                <p align="justify">Neste Projeto conterá os componente Button, Modal, Vertical Menu, com alguns exemplos de utilização e ajuda para utiliza da classe criada.</p>

            </div>
        </div>
    </div>

</main>