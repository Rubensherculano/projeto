<?php

/**
* Classe com o teste dos dados que serão usados em um modal
*/
class ModalTest{
    private $titulo;
    private $nome;
    private $tamanho;
    private $position;
    private $efeito;
    private $conteudo;
    
    function __construct($item) {
        $this->titulo = isset($item->titulo) ? $item->titulo : null;
        $this->nome = isset($item->nome) ? $item->nome : null ;
        $this->tamanho = isset($item->tamanho) ? $item->tamanho : null;
        $this->position = isset($item->position) ? $item->position : null;
        $this->efeito = isset($item->efeito) ? $item->efeito : null;
        $this->conteudo = isset($item->conteudo) ? $item->conteudo : null;
    }
    
    /**
     * Realiza testes com a variavel titulo.
     * @return boolean
     */
    public function verifica_titulo()
    {
        if(!empty($this->titulo) && strlen($this->titulo) > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel nome.
     * @return boolean
     */
    public function verifica_nome()
    {
        if(!empty($this->nome) && strlen($this->nome) > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel tamanho.
     * @return boolean
     */
    public function verifica_tamanho()
    {
        if(!empty($this->tamanho) && strlen($this->tamanho) > 5)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel position.
     * @return boolean
     */
    public function verifica_position()
    {
        if(!empty($this->position) && strlen($this->position) > 5)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel efeito.
     * @return boolean
     */
    public function verifica_efeito()
    {
        if(!empty($this->efeito) && strlen($this->efeito) > 5)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Realiza testes com a variavel conteudo.
     * @return boolean
     */
    public function verifica_conteudo()
    {
        if($this->conteudo != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Monta um card com as informações testada da classe e seus resultados.
     * @param $dados : array dados do modal
     * @return $html : string
     */
    public function get_test($dados)
    {
        $html = '';
        
        $html .= '<div class="col-md-6">';
        $html .= '<div class="card border-dark mr-1 ml-1 mt-3 mb-3">';
        $html .= '<div class="card-body">';
        $html .= '<h5 class="card-title text-primary">Modal: '.$dados->titulo_modal.'</h5>';
        $html .= '<p class="card-title">Titulo: '.($dados->titulo == true ? '<span class="text-primary">Passo - Titulo Preenchido e com 3 caracteres pelo menos</span>' : '<span class="text-danger">O titulo esta vazio ou com menos que 3 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Nome: '.($dados->nome == true ? '<span class="text-primary">Passo - Nome Preenchido e com 3 caracteres pelo menos</span>' : '<span class="text-danger">A nome esta vazia ou com menos que 3 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Tamanho: '.($dados->tamanho == true ? '<span class="text-primary">Passo - Tamanho Preenchido e com 5 caracteres pelo menos</span>' : '<span class="text-danger">O tamanho esta vazia ou com menos que 5 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Position: '.($dados->position == true ? '<span class="text-primary">Passo - Position Preenchido e com 5 caracteres pelo menos</span>' : '<span class="text-danger">O position esta vazia ou com menos que 5 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Efeito: '.($dados->efeito == true ? '<span class="text-primary">Passo - Efeito Preenchido e com 4 caracteres pelo menos</span>' : '<span class="text-danger">O efeito está vazio ou com menos que 5 caracteres</span>').'</p>';
        $html .= '<p class="card-title">Tamanho: '.($dados->conteudo == true ? '<span class="text-primary">Passo - O Conteudo esta presente na classe</span>' : '<span class="text-danger">Não foi passado informação para o conteudo do modal (Necessário mesmo que a informação esteja vazia)</span>').'</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        
        return $html;
    }
}