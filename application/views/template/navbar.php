<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark deep-orange darken-1 scrolling-navbar">
        <a class="navbar-brand" href="<?= base_url("Home") ?>"><img src="<?= base_url("assets/img/Logo/logo_transparent.png") ?>" height="30" alt="Logo da Rei dos Consoles" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url("Home") ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url("Home/button") ?>">Button <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url("Home/modal") ?>">Modal <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url("Home/Vertical_menu") ?>">Vertical Menu <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url("Home/phpdocumentor") ?>">PhpDocumentor <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a href="Face" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="nav-item">
                    <a href="Twitter" class="nav-link"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a href="Instagram" class="nav-link"><i class="fab fa-instagram"></i></a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url("Login/form_login") ?>" class="nav-link"><i class="fas fa-users"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<br/>
