<?php

include_once APPPATH.'libraries/Component.php';
include_once 'ModalData.php';

/**
 * Classe para criar um modal com diferentes caracteristicas.
 */
class ModalItem extends Component{

    private $dados;
    
    function __construct(ModalData $dados) {
        $this->dados = $dados;
    }
    
    /**
     * Monta o codigo html de um modal.
     * @return $html : string
     */
    public function getHTML() {
        $html = '';
        $html .= '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$this->dados->nome().'Modal">'.$this->dados->titulo().'</button>';
        $html .= '<div class="modal fade" id="'.$this->dados->nome().'Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        $html .= '<div class="modal-dialog '.$this->dados->tamanho().' '.$this->dados->position().' '.$this->dados->efeito().'" role="document">';
        $html .= '<div class="modal-content">';
        $html .= '<div class="modal-header">';
        $html .= '<h5 class="modal-title" id="exampleModalLabel">'.$this->dados->titulo().'</h5>';
        $html .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $html .= '</div>';
        $html .= '<div class="modal-body">'.$this->dados->conteudo().'</div>';
        $html .= '<div class="modal-footer">';
        $html .= '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary">Save changes</button>';
        $html .= '</div></div></div></div>';
        
        return $html;
    }
    
    /**
     * Montar o texto em html de como forma um modal com determinadas caracteristicas.
     * @return $html : string
     */
    public function getCodigoHTML()
    {
        $html = '';
        $html .= '<span class="indigo-text">&lt;button <span class="blue-text"> type="<span class="red-text">button</span>" class="<span class="red-text">btn btn-primary</span>" data-toggle="<span class="red-text">modal</span>" data-target="<span class="red-text">#'.$this->dados->nome().'Modal</span>"</span>&gt;</span>Launch demo modal<span class="indigo-text">&lt;/button&gt;</span><br/>';
        $html .= '<span class="indigo-text">&lt;div<span class="blue-text"> class="<span class="red-text">modal fade</span>" id="<span class="red-text">'.$this->dados->nome().'Modal</span>" tabindex="<span class="red-text">-1</span>" role="<span class="red-text">dialog</span>" aria-labelledby="<span class="red-text">exampleModalLabel</span>" aria-hidden="<span class="red-text">true</span>"</span>&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;div<span class="blue-text"> class="<span class="red-text">modal-dialog '.$this->dados->tamanho().' '.$this->dados->position().' '.$this->dados->efeito().'</span>" role="<span class="red-text">document</span>"</span>&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;div<span class="blue-text"> class="<span class="red-text">modal-content</span>"</span>&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;div<span class="blue-text"> class="<span class="red-text">modal-header</span>"</span>&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;h5<span class="blue-text"> class="<span class="red-text">modal-title</span>" id="<span class="red-text">exampleModalLabel</span>"</span>&gt;</span>'.$this->dados->titulo().'<span class="indigo-text">&lt;/h5&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;button<span class="blue-text"> type="<span class="red-text">button</span>" class="<span class="red-text">close</span>" data-dismiss="<span class="red-text">modal</span>" aria-label="<span class="red-text">Close</span>"&gt;</span>&lt;span<span class="blue-text"> aria-hidden="<span class="red-text">true</span>"</span>&gt;&times;&lt;/span&gt;&lt;/button&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;/div&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;div<span class="blue-text"> class="<span class="red-text">modal-body</span>"</span>&gt;</span>'.$this->dados->conteudo().'<span class="indigo-text">&lt;/div&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;div<span class="blue-text"> class="<span class="red-text">modal-footer</span>"</span>&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;button<span class="blue-text"> type="<span class="red-text">button</span>" class="<span class="red-text">btn btn-secondary</span>" data-dismiss="<span class="red-text">modal</span>"</span>&gt;</span>Close<span class="indigo-text">&lt;/button&gt;<br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;button<span class="blue-text"> type="<span class="red-text">button</span>" class="<span class="red-text">btn btn-primary</span>"</span>&gt;</span></span>Save changes<span class="indigo-text">&lt;/button&gt;</span><br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="indigo-text">&lt;/div&gt;<br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt/div&gt<br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt/div&gt<br/>';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt/div&gt</span><br/><br/><br/>';
        
        return $html;
    }
    
    /**
     * Monta um card com informaçõesd dos dados de um modal que foram passadas para a classe.
     * @return $html : string
     */
    public function get_Dados()
    {
        $html = '';
        
        $html .= '<div class="col-md-6">';
        $html .= '<div class="card border-dark mr-1 ml-1 mt-3 mb-3">';
        $html .= '<div class="card-body">';
        $html .= '<h5 class="card-title text-primary">'.$this->dados->titulo().'</h5>';
        $html .= '<p class="card-title">Tituto - '.$this->dados->titulo().'</p>';
        $html .= '<p class="card-title">Nome - '.$this->dados->nome().'</p>';
        $html .= '<p class="card-title">Tamanho - '.$this->dados->tamanho().'</p>';
        $html .= '<p class="card-title">Position - '.$this->dados->position().'</p>';
        $html .= '<p class="card-title">Efeito - '.$this->dados->efeito().'</p>';
        $html .= '<p class="card-title">conteudo - '.$this->dados->conteudo().'</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        
        return $html;
    }
    
}