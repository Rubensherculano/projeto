<?php

include_once APPPATH.'libraries/Component.php';
include_once 'ButtonData.php';

/**
 * Classe para criar um botão com diferentes caracteristicas.
 */
class ButtonItem extends Component{

    private $dados;
    
    function __construct(ButtonData $dados) {
        $this->dados = $dados;
    }
    
    /**
     * Monta o codigo html de um botão.
     * @return $html : string
     */
    public function getHTML()
    {
        $html = '<button';
        $html .= ' type="'.$this->dados->type().'"';
        $html .= ' class="'.$this->dados->classe()." ".$this->dados->size().'"';
        $html .= $this->dados->active() == 1 ? ' aria-pressed="true"' : '';
        $html .= $this->dados->disabled() == 1 ? ' disabled>' : '>';
        $html .= ' '.$this->dados->icon_left();
        $html .= ' '.$this->dados->titulo();
        $html .= ' '.$this->dados->icon_right();
        
        $html .= '</button>';
        
        return $html;
    }
    
    /**
     * Montar o texto em html de como formar um botão com determinadas caracteristicas.
     * @return $html : string
     */
    public function getCodigoHTML()
    {
        $html = '';
        $html .= '<br/>     <span class="grey-text">&lt!-- '.$this->dados->comentario().' --&gt</span><br/>     ';
        $html .= '<span class="indigo-text">&lt;button';
        $html .= '<span class="blue-text"> type="<span class="red-text">'.$this->dados->type().'</span>"</span>';
        $html .= '<span class="blue-text"> class="<span class="red-text">'.$this->dados->classe()." ".$this->dados->size().'</span>"</span>';
        $html .= $this->dados->active() == 1 ? '<span class="blue-text"> aria-pressed="<span class="red-text">true</span>"</span>' : '';
        $html .= $this->dados->disabled() == 1 ? '<span class="blue-text"> disabled</span>&gt;</span>' : '&gt;</span>';
        $html .= ''.!empty($this->dados->icon_left()) ? $this->get_icon($this->dados->icon_left()) : "";
        $html .= ''.$this->dados->titulo();
        $html .= ''.!empty($this->dados->icon_right()) ? $this->get_icon($this->dados->icon_right()) : "";
        $html .= '<span class="indigo-text">&lt;/button&gt;</span>';
        
        return $html;
    }
    
    /**
     * Monta um card com informações que foram passadas para a classe.
     * @return $html : string
     */
    public function get_Dados()
    {
        $html = '';
        
        $html .= '<div class="col-md-6">';
        $html .= '<div class="card border-dark mr-1 ml-1 mt-3 mb-3">';
        $html .= '<div class="card-body">';
        $html .= '<h5 class="card-title text-primary">'.$this->dados->titulo().'</h5>';
        $html .= '<p class="card-title">Tituto - '.$this->dados->titulo().'</p>';
        $html .= '<p class="card-title">Classe - '.$this->dados->classe().'</p>';
        $html .= '<p class="card-title">Icone Esquerdo - '.$this->dados->icon_left().'</p>';
        $html .= '<p class="card-title">Icone Direito - '.$this->dados->icon_right().'</p>';
        $html .= '<p class="card-title">Tipo - '.$this->dados->type().'</p>';
        $html .= '<p class="card-title">Tamanho - '.$this->dados->size().'</p>';
        $html .= '<p class="card-title">Comentario - '.$this->dados->comentario().'</p>';
        $html .= '<p class="card-title">Active - '.($this->dados->active() == 1 ? "true" : "false").'</p>';
        $html .= '<p class="card-title">Disabled - '.($this->dados->disabled() == 1 ? "true" : "false").'</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        
        return $html;
    }
    
    /**
     * Monta o texto de como um icone deve ser inserido no botão.
     * @param string icone a ser transforma em texto html.
     * @return $html : string
     */
    private function get_icon($icon)
    {
        $html = '';
        $html = '<span class="indigo-text">&lti<span class="blue-text"> class="<span class="red-text">'.explode("'", $icon)[1].'</span>"</span>&gt&lt/i&gt</span">';
        
        return $html;
    }
}