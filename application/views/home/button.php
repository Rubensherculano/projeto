<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="section-heading h2 mb-3">Button</h4>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-info" href="<?= base_url("test/Button_Test") ?>">Teste do PHP</a>
                    </div>
                </div>
                <p class="description">Você pode utilizar estes botões custumizados para intereações em formulário, mensagens, e muito mais, tendo suporte a diferentes tamanhos, cores, estados e mais...</p>
                
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Botões Basicos</h5>
                <br/>
                <p class="description">Cores e atribuitos basicos do Botão.</p>
                <p class="description">A partir das classe defini-se a cor do botão.</p>
                <p class="description"><span class="pink-text">.btn-primary</span> - Cor Primary.</p>
                <p class="description"><span class="pink-text">.btn-default</span> - Cor Default.</p>
                <p class="description"><span class="pink-text">.btn-secondary</span> - Cor Secondary.</p>
                <p class="description"><span class="pink-text">.btn-success</span> - Cor Success.</p>
                <p class="description"><span class="pink-text">.btn-warning</span> - Cor Warning.</p>
                <p class="description"><span class="pink-text">.btn-danger</span> - Cor Danger.</p>
                <p class="description">Adicione a btn btn-[cor desejada] para definir a cor do botão.</p>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $buttons_basicos->button ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="basic-button-html-ex" data-toggle="tab" href="#basic-button-html" role="tab" aria-controls="basic-button-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="basic-button-classe-ex" data-toggle="tab" href="#basic-button-classe" role="tab" aria-controls="basic-button-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="basic-button-test-ex" data-toggle="tab" href="#basic-button-test" role="tab" aria-controls="basic-button-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="basic-button-html" role="tabpanel" aria-labelledby="basic-button-html-ex">
                            <?= $buttons_basicos->code ?>
                        </div>
                        <div class="tab-pane fade" id="basic-button-classe" role="tabpanel" aria-labelledby="basic-button-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $buttons_basicos->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="basic-button-test" role="tabpanel" aria-labelledby="basic-button-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($buttons_basicos->test as $item): ?>
                                <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Botões Adicionais</h5>
                <br/>
                <p class="description">Cores adicionais.</p>
                <p class="description">Utilização igual a do anterior.</p>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $buttons_adicionais->button ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="additional-button-html-ex" data-toggle="tab" href="#additional-button-html" role="tab" aria-controls="additional-button-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="additional-button-classe-ex" data-toggle="tab" href="#additional-button-classe" role="tab" aria-controls="additional-button-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="additional-button-test-ex" data-toggle="tab" href="#additional-button-test" role="tab" aria-controls="additional-button-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="additional-button-html" role="tabpanel" aria-labelledby="additional-button-html-ex">
                            <?= $buttons_adicionais->code ?>
                        </div>
                        <div class="tab-pane fade" id="additional-button-classe" role="tabpanel" aria-labelledby="additional-button-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $buttons_adicionais->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="additional-button-test" role="tabpanel" aria-labelledby="additional-button-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($buttons_adicionais->test as $item): ?>
                                <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Botões Gradient</h5>
                <br/>
                <p class="description">Botões com cores gradient.</p>
                <p class="description"><span class="pink-text">.peach-gradient</span> - Cor Peach.</p>
                <p class="description"><span class="pink-text">.purple-gradient</span> - Cor Purple.</p>
                <p class="description"><span class="pink-text">.blue-gradient</span> - Cor Blue.</p>
                <p class="description"><span class="pink-text">.aqua-gradient</span> - Cor Aqua.</p>
                <p class="description">Adicione btn [cor desejada]-gradient para alterar a cor do botão(check a disponibilidade de cores).</p>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $buttons_gradient->button ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="gradient-button-html-ex" data-toggle="tab" href="#gradient-button-html" role="tab" aria-controls="gradient-button-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="gradient-button-classe-ex" data-toggle="tab" href="#gradient-button-classe" role="tab" aria-controls="gradient-button-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="gradient-button-test-ex" data-toggle="tab" href="#gradient-button-test" role="tab" aria-controls="gradient-button-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="gradient-button-html" role="tabpanel" aria-labelledby="gradient-button-html-ex">
                            <?= $buttons_gradient->code ?>
                        </div>
                        <div class="tab-pane fade" id="gradient-button-classe" role="tabpanel" aria-labelledby="gradient-button-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $buttons_gradient->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="gradient-button-test" role="tabpanel" aria-labelledby="gradient-button-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($buttons_gradient->test as $item): ?>
                                <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Botões Outline</h5>
                <br/>
                <p class="description">Botão com cores, mas sem preenchimento.</p>
                <p class="description"><span class="pink-text">.btn-outline-primary</span> - Cor Primary.</p>
                <p class="description"><span class="pink-text">.btn-outline-Default</span> - Cor Default.</p>
                <p class="description"><span class="pink-text">.btn-outline-Secondary</span> - Cor Secondary.</p>
                <p class="description"><span class="pink-text">.btn-outline-Success</span> - Cor Success.</p>
                <p class="description"><span class="pink-text">.btn-outline-info</span> - Cor Info.</p>
                <p class="description"><span class="pink-text">.btn-outline-warning</span> - Cor Warning.</p>
                <p class="description"><span class="pink-text">.btn-outline-danger</span> - Cor Danger.</p>
                <p class="description">Para adicionar o este efeito utilize a classe btn btn-outline-[cor desejada].</p>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $buttons_outline->button ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="outline-button-html-ex" data-toggle="tab" href="#outline-button-html" role="tab" aria-controls="outline-button-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="outline-button-classe-ex" data-toggle="tab" href="#outline-button-classe" role="tab" aria-controls="outline-button-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="outline-button-test-ex" data-toggle="tab" href="#outline-button-test" role="tab" aria-controls="outline-button-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="outline-button-html" role="tabpanel" aria-labelledby="outline-button-html-ex">
                            <?= $buttons_outline->code ?>
                        </div>
                        <div class="tab-pane fade" id="outline-button-classe" role="tabpanel" aria-labelledby="outline-button-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $buttons_outline->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="outline-button-test" role="tabpanel" aria-labelledby="outline-button-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($buttons_outline->test as $item): ?>
                                <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Botões Icon</h5>
                <br/>
                <p class="description">Botões de cores e com icones.</p>
                <p class="description">&lt;button&gt;<span class="pink-text">&lt;i class=""&gt;&lt;/i&gt;</span>nome_botão&lt;/button&gt; - Icone no botão a esquerda.</p>
                <p class="description">&lt;button&gt;nome_botão<span class="pink-text">&lt;i class=""&gt;&lt;/i&gt;</span>&lt;/button&gt; - Icone no botão a esquerda.</p>
                <p class="description">Para adicionar o icone apenas coloque o codigo do icone na classe de acordo com a posição do icone desejado.</p>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $buttons_icon->button ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="icon-button-html-ex" data-toggle="tab" href="#icon-button-html" role="tab" aria-controls="icon-button-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="icon-button-classe-ex" data-toggle="tab" href="#icon-button-classe" role="tab" aria-controls="icon-button-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="icon-button-test-ex" data-toggle="tab" href="#icon-button-test" role="tab" aria-controls="icon-button-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="icon-button-html" role="tabpanel" aria-labelledby="icon-button-html-ex">
                    <?= $buttons_icon->code ?>
                        </div>
                        <div class="tab-pane fade" id="icon-button-classe" role="tabpanel" aria-labelledby="icon-button-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $buttons_icon->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="icon-button-test" role="tabpanel" aria-labelledby="icon-button-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($buttons_icon->test as $item): ?>
                                <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Tamanho dos botões</h5>
                <br/>
                <p class="description">Defina o tamanho dos botoões.</p>
                <p class="description"><span class="pink-text">.btn-lg</span> - Botão Grande.</p>
                <p class="description"><span class="pink-text">.btn-sm</span> - Botão Pequeno.</p>
                <p class="description"><span class="pink-text">.btn-block</span> - Botão Bloco.</p>
                <p class="description">Para definir o tamanho adicione btn-[tamanho desejado] na classe.</p>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $buttons_size->button ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="size-button-html-ex" data-toggle="tab" href="#size-button-html" role="tab" aria-controls="size-button-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="size-button-classe-ex" data-toggle="tab" href="#size-button-classe" role="tab" aria-controls="size-button-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="size-button-test-ex" data-toggle="tab" href="#size-button-test" role="tab" aria-controls="size-button-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="size-button-html" role="tabpanel" aria-labelledby="size-button-html-ex">
                            <?= $buttons_size->code ?>
                        </div>
                        <div class="tab-pane fade" id="size-button-classe" role="tabpanel" aria-labelledby="size-button-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $buttons_size->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="size-button-test" role="tabpanel" aria-labelledby="size-button-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($buttons_size->test as $item): ?>
                                <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Status Ativo</h5>
                <br/>
                <p class="description">Deixa o botão com aparencia de clicado.</p>
                <p class="description"><span class="pink-text">aria-pressed="true"</span> - Ativo.</p>
                <p class="description">Para deixar com efeito ativo adicione true na classe na area de active.</p>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $buttons_active->button ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="active-button-html-ex" data-toggle="tab" href="#active-button-html" role="tab" aria-controls="active-button-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="active-button-classe-ex" data-toggle="tab" href="#active-button-classe" role="tab" aria-controls="active-button-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="active-button-test-ex" data-toggle="tab" href="#active-button-test" role="tab" aria-controls="active-button-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="active-button-html" role="tabpanel" aria-labelledby="active-button-html-ex">
                            <?= $buttons_active->code ?>
                        </div>
                        <div class="tab-pane fade" id="active-button-classe" role="tabpanel" aria-labelledby="active-button-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $buttons_active->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="active-button-test" role="tabpanel" aria-labelledby="active-button-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($buttons_active->test as $item): ?>
                                <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Status Desativado</h5>
                <br/>
                <p class="description">Deixa o botão com aparencia de desativado.</p>
                <p class="description"><span class="pink-text">disabled</span> - Desativado.</p>
                <p class="description">Para deixar com efeito desativado adicione true na classe na area de disabled.</p>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $buttons_disabled->button ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="disabled-button-html-ex" data-toggle="tab" href="#disabled-button-html" role="tab" aria-controls="disabled-button-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="disabled-button-classe-ex" data-toggle="tab" href="#disabled-button-classe" role="tab" aria-controls="disabled-button-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="disabled-button-test-ex" data-toggle="tab" href="#disabled-button-test" role="tab" aria-controls="disabled-button-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="disabled-button-html" role="tabpanel" aria-labelledby="disabled-button-html-ex">
                            <?= $buttons_disabled->code ?>
                        </div>
                        <div class="tab-pane fade" id="disabled-button-classe" role="tabpanel" aria-labelledby="disabled-button-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $buttons_disabled->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="disabled-button-test" role="tabpanel" aria-labelledby="disabled-button-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($buttons_disabled->test as $item): ?>
                                <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>