<?php

/**
 * Classe para receber e utilizar o dados para criar um botão.
 */
class ButtonData{
    private $titulo;
    private $classe;
    private $comentario;
    private $icon_left;
    private $icon_right;
    private $type;
    private $size;
    private $active;
    private $disabled;
    
    function __construct($item) {
        $this->titulo = isset($item->titulo) ? $item->titulo : null;
        $this->classe = isset($item->classe) ? $item->classe : null;
        $this->comentario = isset($item->comentario) ? $item->comentario : null;
        $this->icon_left = isset($item->icon_left) ? $item->icon_left : null;
        $this->icon_right = isset($item->icon_right) ? $item->icon_right : null;
        $this->type = isset($item->type) ? $item->type : null;
        $this->size = isset($item->size) ? $item->size : null;
        $this->active = isset($item->active) ? $item->active : null;
        $this->disabled = isset($item->disabled) ? $item->disabled : null;
    }
    
    /**
     * Retorna o valor armazenado em titulo nesta classe.
     * @return string
     */
    public function titulo()
    {
        return $this->titulo;
    }
    
    /**
     * Retorna o valor armazenado em classe nesta classe.
     * @return string
     */
    public function classe()
    {
        return $this->classe;
    }
    
    /**
     * Retorna o valor armazenado em comentario nesta classe.
     * @return string
     */
    public function comentario()
    {
        return $this->comentario;
    }
    
    /**
     * Retorna o valor armazenado em icon_left nesta classe.
     * @return string
     */
    public function icon_left()
    {
        return $this->icon_left;
    }
    
    /**
     * Retorna o valor armazenado em icon_right nesta classe.
     * @return string
     */
    public function icon_right()
    {
        return $this->icon_right;
    }
    
    /**
     * Retorna o valor armazenado em type nesta classe.
     * @return string
     */
    public function type()
    {
        return $this->type;
    }
    
    /**
     * Retorna o valor armazenado em size nesta classe.
     * @return string
     */
    public function size()
    {
        return $this->size;
    }
    
    /**
     * Retorna o valor armazenado em active nesta classe.
     * @return string
     */
    public function active()
    {
        return $this->active;
    }
    
    /**
     * Retorna o valor armazenado em disabled nesta classe.
     * @return string
     */
    public function disabled()
    {
        return $this->disabled;
    }
    
}