<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="section-heading h2 mb-3">Modal</h4>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-info" href="<?= base_url("test/Modal_Test") ?>">Teste do PHP</a>
                    </div>
                </div>
                <p class="description">O Modal é uma caixa de diálogo / janela pop-up que pode ser usada para LightBox, notificações do usuário, aprimoramentos da interface do usuário, componentes de comércio eletrônico e muitos outros casos.</p>
                <p class="description">É facilmente personalizado. Você pode manipular tamanho, posição e conteúdo..</p>
                
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Tipo de Modal</h5>
                <p class="description">Defina o tamanho do modal que deseja utlizar.</p>
                <p class="description"><span class="pink-text">.modal-md</span> - Modal de tamanho Medio.</p>
                <p class="description"><span class="pink-text">.modal-sm</span> - Modal de tamanho Pequeno.</p>
                <p class="description"><span class="pink-text">.modal-lg</span> - Modal de tamanho Grande.</p>
                <p class="description"><span class="pink-text">.modal-fluid</span> - Modal de tamanho Fluid(largura da tela).</p>
                <p class="description">Adicione o valor desejado de tamanho em tamanho na classe.</p>
                <br/>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $modal_type->modal ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="modal-type-html-ex" data-toggle="tab" href="#modal-type-html" role="tab" aria-controls="modal-type-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="modal-type-classe-ex" data-toggle="tab" href="#modal-type-classe" role="tab" aria-controls="modal-type-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="modal-type-test-ex" data-toggle="tab" href="#modal-type-test" role="tab" aria-controls="modal-type-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="modal-type-html" role="tabpanel" aria-labelledby="modal-type-html-ex">
                            <?= $modal_type->code ?>
                        </div>
                        <div class="tab-pane fade" id="modal-type-classe" role="tabpanel" aria-labelledby="modal-type-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $modal_type->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="modal-type-test" role="tabpanel" aria-labelledby="modal-type-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($modal_type->test as $item): ?>
                                    <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Modals Laterais</h5>
                <p class="description">Um tipo de posição em que o modal pode estar.</p>
                <p class="description"><span class="pink-text">.modal-side</span> + <span class="pink-text">.modal-top-right</span> - Modal no canto superior a direita.</p>
                <p class="description"><span class="pink-text">.modal-side</span> + <span class="pink-text">.modal-top-left </span> - Modal no canto superior a esquerda.</p>
                <p class="description"><span class="pink-text">.modal-side</span> + <span class="pink-text">.modal-bottom-right</span> - Modal no canto inferior a direita.</p>
                <p class="description"><span class="pink-text">.modal-side</span> + <span class="pink-text">.modal-bottom-left</span> - Modal no canto inferior a esquerda.</p>
                <p class="description">Adicione o valor desejado de tamanho em tamanho na classe.</p>
                <br/>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $modal_side->modal ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="modal-side-html-ex" data-toggle="tab" href="#modal-side-html" role="tab" aria-controls="modal-side-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="modal-side-classe-ex" data-toggle="tab" href="#modal-side-classe" role="tab" aria-controls="modal-side-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="modal-side-test-ex" data-toggle="tab" href="#modal-side-test" role="tab" aria-controls="modal-side-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="modal-side-html" role="tabpanel" aria-labelledby="modal-side-html-ex">
                            <?= $modal_side->code ?>
                        </div>
                        <div class="tab-pane fade" id="modal-side-classe" role="tabpanel" aria-labelledby="modal-side-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $modal_side->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="modal-side-test" role="tabpanel" aria-labelledby="modal-side-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($modal_side->test as $item): ?>
                                    <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Modals Fluid (preenchimento total da medidad)</h5>
                <p class="description">Um tipo de posição em que o modal pode estar.</p>
                <p class="description"><span class="pink-text">.modal-full-height</span> + <span class="pink-text">.modal-right</span> - Modal no canto Direita.</p>
                <p class="description"><span class="pink-text">.modal-full-height</span> + <span class="pink-text">.modal-left </span> - Modal no canto Esquerda.</p>
                <p class="description"><span class="pink-text">.modal-full-height</span> + <span class="pink-text">.modal-bottom</span> - Modal na area Inferior .</p>
                <p class="description"><span class="pink-text">.modal-full-height</span> + <span class="pink-text">.modal-top</span> - Modal na area Superior.</p>
                <p class="description">Adicione o valor desejado de posição do modal e adicione em efeito a classe (modal-full-height), e adicione a local do modal desejado em position.</p>
                <br/>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $modal_fluid->modal ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="modal-fluid-html-ex" data-toggle="tab" href="#modal-fluid-html" role="tab" aria-controls="modal-fluid-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="modal-fluid-classe-ex" data-toggle="tab" href="#modal-fluid-classe" role="tab" aria-controls="modal-fluid-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="modal-fluid-test-ex" data-toggle="tab" href="#modal-fluid-test" role="tab" aria-controls="modal-fluid-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="modal-fluid-html" role="tabpanel" aria-labelledby="modal-fluid-html-ex">
                            <?= $modal_fluid->code ?>
                        </div>
                        <div class="tab-pane fade" id="modal-fluid-classe" role="tabpanel" aria-labelledby="modal-fluid-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $modal_fluid->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="modal-fluid-test" role="tabpanel" aria-labelledby="modal-fluid-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($modal_fluid->test as $item): ?>
                                    <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <hr class="my-5">
                <h5 class="section-heading h4 mb-2 text-capitalize">Modals Frame (preenchimento total da largura)</h5>
                <p class="description">Um tipo de posição em que o modal pode estar.</p>
                <p class="description"><span class="pink-text">.modal-frame</span> + <span class="pink-text">.modal-right</span> - Modal no canto Direita.</p>
                <p class="description"><span class="pink-text">.modal-frame</span> + <span class="pink-text">.modal-left </span> - Modal no canto Esquerda.</p>
                <p class="description"><span class="pink-text">.modal-frame</span> + <span class="pink-text">.modal-bottom</span> - Modal na area Inferior .</p>
                <p class="description"><span class="pink-text">.modal-frame</span> + <span class="pink-text">.modal-top</span> - Modal na area Superior.</p>
                <p class="description">Adicione o valor desejado de posição do modal e adicione em efeito a classe (modal-frame), e adicione a local do modal desejado em position.</p>
                <br/>
                <div class="row border rounded-sm mb-3">
                    <div class="mb-3 mt-3 ml-3">
                        <?= $modal_frame->modal ?>
                    </div>    
                </div>
                <div class="row border grey lighten-5">
                    <div class="mb-2 mt-2 ml-3">
                        <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm show" id="modal-frame-html-ex" data-toggle="tab" href="#modal-frame-html" role="tab" aria-controls="modal-frame-html" aria-selected="true">HTML</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="modal-frame-classe-ex" data-toggle="tab" href="#modal-frame-classe" role="tab" aria-controls="modal-frame-classe" aria-selected="false">Classe</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-blue-grey btn-sm" id="modal-frame-test-ex" data-toggle="tab" href="#modal-frame-test" role="tab" aria-controls="modal-frame-test" aria-selected="false">Teste</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row border rounded-sm grey lighten-3 mb-5">
                    <div class="tab-content" id="myTabContentEx">
                        <div class="tab-pane fade active ml-3 mr-3 show" id="modal-frame-html" role="tabpanel" aria-labelledby="modal-frame-html-ex">
                            <?= $modal_frame->code ?>
                        </div>
                        <div class="tab-pane fade" id="modal-frame-classe" role="tabpanel" aria-labelledby="modal-frame-classe-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?= $modal_frame->dados ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="modal-frame-test" role="tabpanel" aria-labelledby="modal-frame-test-ex">
                            <div class="row mr-3 ml-3 mt-3 mb-3">
                                <?php foreach($modal_frame->test as $item): ?>
                                    <?= $item->html; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>