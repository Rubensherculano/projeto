<?php

include_once 'ButtonItem.php';
include_once 'ButtonData.php';
include_once 'ButtonTest.php';

/**
 * Classe para criar blocos de botões diferentes, onde são passandos os dados do botão para as classes de criação.
 */
class ButtonLoader{

    private $buttonData;
    private $test;

    /**
     * Monta a estrutura dos botões basicos(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosBotao_Basicos()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_botoes_basicos();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->classe == false || $test[$key]->test->comentario == false || $test[$key]->test->icon_left == false || $test[$key]->test->icon_right == false || $test[$key]->test->type == false ||  $test[$key]->test->size == false)
            {
                $verf = $key;
            }
        }
        
        $this->buttonData = $array;
        $this->test = $test;
        
        $html = '';
        
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $button = new ButtonData($item);
                $component = new ButtonItem($button);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
    }
    
    /**
     * Monta a estrutura dos botões Adicionais(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosBotao_Adicionais()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_botoes_adicionais();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->classe == false || $test[$key]->test->comentario == false || $test[$key]->test->icon_left == false || $test[$key]->test->icon_right == false || $test[$key]->test->type == false ||  $test[$key]->test->size == false)
            {
                $verf = $key;
            }
        }
        
        $this->buttonData = $array;
        $this->test = $test;

        $html = '';
        
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $button = new ButtonData($item);
                $component = new ButtonItem($button);
                $html .= $component->getHTML();
            }
        }
        
        return $html;   
    }
    
    /**
     * Monta a estrutura dos botões Gradientes(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosBotao_Gradient()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_botoes_gradient();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->classe == false || $test[$key]->test->comentario == false || $test[$key]->test->icon_left == false || $test[$key]->test->icon_right == false || $test[$key]->test->type == false ||  $test[$key]->test->size == false)
            {
                $verf = $key;
            }
        }
        
        $this->buttonData = $array;
        $this->test = $test;

        $html = '';
        
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $button = new ButtonData($item);
                $component = new ButtonItem($button);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
        
    }
    
    /**
     * Monta a estrutura dos botões sem preenchimento de cor(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosBotao_Outline()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_botoes_outline();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->classe == false || $test[$key]->test->comentario == false || $test[$key]->test->icon_left == false || $test[$key]->test->icon_right == false || $test[$key]->test->type == false ||  $test[$key]->test->size == false)
            {
                $verf = $key;
            }
        }
        
        $this->buttonData = $array;
        $this->test = $test;

        $html = '';
        
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $button = new ButtonData($item);
                $component = new ButtonItem($button);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
        
    }
    
    /**
     * Monta a estrutura dos botões de icone(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosBotao_Icon()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_botoes_icon();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->classe == false || $test[$key]->test->comentario == false || $test[$key]->test->icon_left == false || $test[$key]->test->icon_right == false || $test[$key]->test->type == false ||  $test[$key]->test->size == false)
            {
                $verf= $key;
            }
        }
        
        $this->buttonData = $array;
        $this->test = $test;

        $html = '';
        
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $button = new ButtonData($item);
                $component = new ButtonItem($button);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
        
    }
    
    /**
     * Monta a estrutura dos botões de tamanho diferentes(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosBotao_Size()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_botoes_size();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->classe == false || $test[$key]->test->comentario == false || $test[$key]->test->icon_left == false || $test[$key]->test->icon_right == false || $test[$key]->test->type == false ||  $test[$key]->test->size == false)
            {
                $verf = $key;
            }
        }
        
        $this->buttonData = $array;
        $this->test = $test;

        $html = '';
        
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $button = new ButtonData($item);
                $component = new ButtonItem($button);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
        
    }
    
    /**
     * Monta a estrutura dos botões de com efeito ativo(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosBotao_Active()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_botoes_active();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->classe == false || $test[$key]->test->comentario == false || $test[$key]->test->icon_left == false || $test[$key]->test->icon_right == false || $test[$key]->test->type == false ||  $test[$key]->test->size == false)
            {
                $verf = $key;
            }
        }
        
        $this->buttonData = $array;
        $this->test = $test;

        $html = '';
        
        foreach($array as $key => $item)
        {
            if($key != $verf)
            {
                $button = new ButtonData($item);
                $component = new ButtonItem($button);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
        
    }
    
    /**
     * Monta a estrutura dos botões de efeito desativado(os que aparencem no site)
     * @return $html: string
     */
    public function getDadosBotao_Disabled()
    {
        $verf = -1;
        $test = array();
        $array = $this->get_botoes_disabled();
        
        foreach($array as $key => $item)
        {
            $test[$key] = $this->verifica_test($item);
            if($test[$key]->test->titulo == false || $test[$key]->test->classe == false || $test[$key]->test->comentario == false || $test[$key]->test->icon_left == false || $test[$key]->test->icon_right == false || $test[$key]->test->type == false ||  $test[$key]->test->size == false)
            {
                $verf = $key;
            }
        }
        
        $this->buttonData = $array;
        $this->test = $test;

        $html = '';
        
        foreach($array as $item)
        {
            if($key != $verf)
            {
                $button = new ButtonData($item);
                $component = new ButtonItem($button);
                $html .= $component->getHTML();
            }
        }
        
        return $html;
        
    }
    
    /**
     * Pega o codigo HTML das estruturas de cada botão, para mostrar como linha de codigo.
     * @return $html: string
     */
    public function gera_codigo_HTML()
    {
        $html = '';
        foreach($this->buttonData as $item)
        {
            $button = new ButtonData($item);
            $component = new ButtonItem($button);
            $html .= $component->getCodigoHTML();
        }
        
        return $html;
    }
    
    /**
     * Pega o card com informações que foram utilizadas na classe.
     * @return $html: string
     */
    public function get_dados()
    {
        $html = '';
        
        foreach($this->buttonData as $item)
        {
            $button = new ButtonData($item);
            $component = new ButtonItem($button);
            $html .= $component->get_Dados();
        }

        return $html;
    }
    
    /**
     * Pega o card com as informações que foram passada para a classe, mostrando no resultado o que foi preenchido.
     * @param $dados: array variavel com os dados de um botão.
     * @return $html: string
     */
    public function verifica_test($dados)
    {
        $html =  (object)array();
        $test = new ButtonTest($dados);
        
        $array = (object)array();
        $array->titulo = $test->verifica_titulo();
        $array->classe = $test->verifica_classe();
        $array->comentario = $test->verifica_comentario();
        $array->icon_left = $test->verifica_icon_left();
        $array->icon_right = $test->verifica_icon_right();
        $array->type = $test->verifica_type();
        $array->size = $test->verifica_size();
        $array->active = $test->verifica_active();
        $array->disabled = $test->verifica_disabled();
        $array->titulo_button = $dados->titulo;
        
        $html->test = $array;
        $html->html = $test->get_test($array);
        
        return $html;
    }
    
    /**
     * Retorna o card do teste de cada botão.
     * @return string
     */
    public function get_test()
    {
        return $this->test;
    }
    
    /**
     * Dados a serem passados para classe de botão, para ficarem com as informações do botão basico.
     * @return $array :array
     */
    public function get_botoes_basicos()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Primary",
                "classe" => "btn btn-primary",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Primary Button"
            ),
            "1" => (object)array(
                "titulo" => "Default",
                "classe" => "btn btn-default",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Default Button"
            ),
            "2" => (object)array(
                "titulo" => "Secondary",
                "classe" => "btn btn-secondary",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Secondary Button"
            ),
            "3" => (object)array(
                "titulo" => "Success",
                "classe" => "btn btn-success",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Success Button"
            ),
            "4" => (object)array(
                "titulo" => "Warning",
                "classe" => "btn btn-warning",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Warning Button"
            ),
            "5" => (object)array(
                "titulo" => "Danger",
                "classe" => "btn btn-danger",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Danger Button"
            )
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de botão, para ficarem com as informações do botão de cores adicionais.
     * @return $array :array
     */
    public function get_botoes_adicionais()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Unique",
                "classe" => "btn btn-unique",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Unique Button, adicione o btn-unique a classe para utilizar a cor."
            ),
            "1" => (object)array(
                "titulo" => "Pink",
                "classe" => "btn btn-pink",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Pink Button, adicione o btn-pink a classe para utilizar a cor"
            ),
            "2" => (object)array(
                "titulo" => "Purple",
                "classe" => "btn btn-purple",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Purple Button, adicione o btn-purple a classe para utilizar a cor"
            ),
            "3" => (object)array(
                "titulo" => "Deep Purple",
                "classe" => "btn btn-deep-purple",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Deep Purple Button, adicione o btn-deep-purple a classe para utilizar a cor"
            ),
            "4" => (object)array(
                "titulo" => "Indigo",
                "classe" => "btn btn-indigo",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Indigo Button, adicione o btn-indigo a classe para utilizar a cor"
            ),
            "5" => (object)array(
                "titulo" => "Light Blue",
                "classe" => "btn btn-light-blue",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Light Blue Button, adicione o btn-light-blue a classe para utilizar a cor"
            ),
            "6" => (object)array(
                "titulo" => "Cyan",
                "classe" => "btn btn-cyan",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Cyan Button, adicione o btn-cyan a classe para utilizar a cor"
            ),
            "7" => (object)array(
                "titulo" => "Dark Green",
                "classe" => "btn btn-dark-green",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Dark Green Button, adicione o btn-dark-green a classe para utilizar a cor"
            ),
            "8" => (object)array(
                "titulo" => "Light Green",
                "classe" => "btn btn-light-green",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Light Green Button, adicione o btn-light-green a classe para utilizar a cor"
            ),
            "9" => (object)array(
                "titulo" => "Yellow",
                "classe" => "btn btn-yellow",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Yellow Button, adicione o btn-yellow a classe para utilizar a cor"
            ),
            "10" => (object)array(
                "titulo" => "Amber",
                "classe" => "btn btn-amber",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Amber Button, adicione o btn-amber a classe para utilizar a cor"
            ),
            "11" => (object)array(
                "titulo" => "Deep Orange",
                "classe" => "btn btn-deep-orange",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Deep Orange Button, adicione o btn-deep-orange a classe para utilizar a cor"
            ),
            "12" => (object)array(
                "titulo" => "Brown",
                "classe" => "btn btn-brown",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Brown Button, adicione o btn-brown a classe para utilizar a cor"
            ),
            "13" => (object)array(
                "titulo" => "Blue Grey",
                "classe" => "btn btn-blue-grey",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Blue Grey Button, adicione o btn-blue-grey a classe para utilizar a cor"
            ),
            "14" => (object)array(
                "titulo" => "MDB Color",
                "classe" => "btn btn-mdb-color",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "MDB Button, adicione o btn-mdb-color a classe para utilizar a cor"
            ),
        );
        
        return $array;
    }

    /**
     * Dados a serem passados para classe de botão, para ficarem com as informações do botão com cores gradiente.
     * @return $array :array
     */
    public function get_botoes_gradient()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Peach",
                "classe" => "btn peach-gradient",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Peach Gradient Button, adicione o btn-peach-gradient a classe para utilizar a cor"
            ),
            "1" => (object)array(
                "titulo" => "Purple",
                "classe" => "btn purple-gradient",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Purple Gradient Button, adicione o btn-purple-gradient a classe para utilizar a cor"
            ),
            "2" => (object)array(
                "titulo" => "Blue",
                "classe" => "btn blue-gradient",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Blue Gradient Button, adicione o btn-blue-gradient a classe para utilizar a cor"
            ),
            "3" => (object)array(
                "titulo" => "Aqua",
                "classe" => "btn aqua-gradient",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Aqua Gradient Button, adicione o btn-aqua-gradient a classe para utilizar a cor"
            )
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de botão, para ficarem com as informações do botão sem preenchimento de cores.
     * @return $array :array
     */
    public function get_botoes_outline()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Primary",
                "classe" => "btn btn-outline-primary waves-effect",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Primary Button com efeito sem preenchimento de cor, adicione o btn btn-outline-primary waves-effect a classe para utilizar a cor"
            ),
            "1" => (object)array(
                "titulo" => "Default",
                "classe" => "btn btn-outline-default waves-effect",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Default Button com efeito sem preenchimento de cor, adicione o btn btn-outline-default waves-effect a classe para utilizar a cor"
            ),
            "2" => (object)array(
                "titulo" => "Secondary",
                "classe" => "btn btn-outline-secondary waves-effect",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Secondary Button com efeito sem preenchimento de cor, adicione o btn btn-outline-secondary waves-effect a classe para utilizar a cor"
            ),
            "3" => (object)array(
                "titulo" => "Success",
                "classe" => "btn btn-outline-success waves-effect",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Success Button com efeito sem preenchimento de cor, adicione o btn btn-outline-success waves-effect a classe para utilizar a cor"
            ),
            "4" => (object)array(
                "titulo" => "Info",
                "classe" => "btn btn-outline-info waves-effect",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Info Button com efeito sem preenchimento de cor, adicione o btn btn-outline-info waves-effect a classe para utilizar a cor"
            ),
            "5" => (object)array(
                "titulo" => "Warning",
                "classe" => "btn btn-outline-warning waves-effect",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Warning Button com efeito sem preenchimento de cor, adicione o btn btn-outline-warning waves-effect a classe para utilizar a cor"
            ),
            "6" => (object)array(
                "titulo" => "Danger",
                "classe" => "btn btn-outline-danger waves-effect",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Danger Button com efeito sem preenchimento de cor, adicione o btn btn-outline-danger waves-effect a classe para utilizar a cor"
            )
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de botão, para ficarem com as informações do botão com icone.
     * @return $array :array
     */
    public function get_botoes_icon()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Left",
                "classe" => "btn btn-primary",
                "icon_left" => "<i class='fas fa-magic mr-1'></i>",
                "icon_right" => "",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Primary Button com icone no lado Esquerdo, adicione o icone desajado na opção da classe que deseja"
            ),
            "1" => (object)array(
                "titulo" => "Right",
                "classe" => "btn btn-default",
                "icon_left" => "",
                "icon_right" => "<i class='fas fa-magic ml-1'></i>",
                "type" => "button",
                "size" => "",
                "active" => false,
                "disabled" => false,
                "comentario" => "Default Button com icone no lado Direito, adicione o icone desajado na opção da classe que deseja"
            )
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de botão, para ficarem com as informações do botão de tamanho diferentes.
     * @return $array :array
     */
    public function get_botoes_size()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Large Button",
                "classe" => "btn btn-default",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "btn-lg",
                "active" => false,
                "disabled" => false,
                "comentario" => "Default Button de um tamanho grande, adicione o tamanho a classe para alterar o tamanho."
            ),
            "1" => (object)array(
                "titulo" => "Small Button",
                "classe" => "btn btn-primary",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "btn-sm",
                "active" => false,
                "disabled" => false,
                "comentario" => "Primary Button de um tamanho pequeno, adicione o tamanho a classe para alterar o tamanho."
            ),
            "2" => (object)array(
                "titulo" => "Block Button",
                "classe" => "btn btn-warning",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "btn-block",
                "active" => false,
                "disabled" => false,
                "comentario" => "Warning Button do tamanho do bloco, adicione o tamanho a classe para alterar o tamanho."
            ),
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de botão, para ficarem com as informações do botão com efeito ativo.
     * @return $array :array
     */
    public function get_botoes_active()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Left",
                "classe" => "btn btn-info active",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "btn-lg",
                "active" => true,
                "disabled" => false,
                "comentario" => "Info Button com o efeito de botão ja clicado."
            ),
            "1" => (object)array(
                "titulo" => "Right",
                "classe" => "btn btn-danger active",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "btn-lg",
                "active" => true,
                "disabled" => false,
                "comentario" => "Danger Button com o efeito de botão ja clicado."
            )
        );
        
        return $array;
    }
    
    /**
     * Dados a serem passados para classe de botão, para ficarem com as informações do botão efeito desativado.
     * @return $array :array
     */
    public function get_botoes_disabled()
    {
        $array = array(
            "0" => (object)array(
                "titulo" => "Left",
                "classe" => "btn btn-info",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "btn-lg",
                "active" => false,
                "disabled" => true,
                "comentario" => "Info Button com o efeito de botão desativado."
            ),
            "1" => (object)array(
                "titulo" => "Right",
                "classe" => "btn btn-danger",
                "icon_left" => "",
                "icon_right" => "",
                "type" => "button",
                "size" => "btn-lg",
                "active" => false,
                "disabled" => true,
                "comentario" => "Danger Button com o efeito de botão desativado"
            )
        );
        
        return $array;
    }
}