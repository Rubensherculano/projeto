<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include_once APPPATH. '/libraries/Modal/ModalLoader.php';

class Modal_Test extends MyToast {
    function __construct() {
        parent::__construct('Modal_Test');
    }
    
    /**
     * Realiza testes com a variavel titulo(controller).
     * @return boolean
     */
    function test_verifica_titulo_modal()
    {
        $button = new ModalLoader();
        $info = $button->get_modal_type()[0];
        $result = strlen($info->titulo) > 3 ? true : false;
        $this->_assert_true($result, "O titulo precisa estar preenchido e ter mais que 3 caracteres");
    }
    
    /**
     * Realiza testes com a variavel nome(controller).
     * @return boolean
     */
    function test_verifica_nome_modal()
    {
        $button = new ModalLoader();
        $info = $button->get_modal_type()[0];
        $result = strlen($info->nome) > 3 ? true : false;
        $this->_assert_true($result, "O nome precisa estar preenchido e ter mais que 3 caracteres");
    }
    
    /**
     * Realiza testes com a variavel tamanho(controller).
     * @return boolean
     */
    function test_verifica_tamanho_modal()
    {
        $button = new ModalLoader();
        $info = $button->get_modal_type()[0];
        $result = strlen($info->tamanho) > 5 ? true : false;
        $this->_assert_true($result, "O tamanho precisa estar preenchido e ter mais que 5 caracteres");
    }
    
    /**
     * Realiza testes com a variavel position(controller).
     * @return boolean
     */
    function test_verifica_position_modal()
    {
        $button = new ModalLoader();
        $info = $button->get_modal_type()[0];
        $result = strlen($info->position) > 5 ? true : false;
        $this->_assert_true($result, "O position precisa estar preenchido e ter mais que 5 caracteres");
    }
    
    /**
     * Realiza testes com a variavel efeito(controller).
     * @return boolean
     */
    function test_verifica_efeito_modal()
    {
        $button = new ModalLoader();
        $info = $button->get_modal_type()[0];
        $result = strlen($info->efeito) > 5 ? true : false;
        $this->_assert_true($result, "O efeito precisa estar preenchido e ter mais que 5 caracteres");
    }
    
    /**
     * Realiza testes com a variavel conteudo(controller).
     * @return boolean
     */
    function test_verifica_conteudo_modal()
    {
        $button = new ModalLoader();
        $info = $button->get_modal_type()[0];
        $result = isset($info->conteudo) ? true : false;
        $this->_assert_true($result, "O conteudo deve existir na classe(mesmo que vazia)");
    }
}
